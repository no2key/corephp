<?php
return array(
'ALL_PASS'=>'ZZFCZJSH',      //旧版加密密钥
'authkey'=>'ksdfji',                    // 加密密钥
'cookiepre'=>'cp',            // cookie 前缀
'cookiedomain'=> '', 			// cookie 作用域
'cookiepath' => '/',			// cookie 作用路径

/* Cookie设置 */
'COOKIE_EXPIRE'         => 0,    // Coodie有效期
'COOKIE_DOMAIN'         => '',      // Cookie有效域名
'COOKIE_PATH'           => '/',     // Cookie路径
'COOKIE_PREFIX'         => '',      // Cookie前缀 避免冲突

//日志和错误调试配置
'DEBUG'=>true,	//是否开启调试模式，true开启，false关闭
'LOG_ON'=>true,//是否开启出错信息保存到文件，true开启，false不开启
'ERROR_URL'=>'',//出错信息重定向页面，为空采用默认的出错页面，一般不需要修改
//分组配置
'GROUP_DEFAULT'=>'manage',//默认分组，设置默认分组后表示开启分组功能
'GROUP_DOMAIN'=>array(
					//'manage'=>'6781.com',					
				),		
'URL_HTML_SUFFIX' => '',//伪静态后缀设置，例如 .html ，一般不需要修改	
//数据库配置
'DB_TYPE'=>'mysql',//数据库类型，一般不需要修改
'DB_HOST'=>'localhost',//数据库主机，一般不需要修改
'DB_USER'=>'root',//数据库用户名
'DB_PWD'=>'root',//数据库密码
'DB_PORT'=>3306,//数据库端口，mysql默认是3306，一般不需要修改
'DB_NAME'=>'corephp',//数据库名
'DB_CHARSET'=>'utf8',//数据库编码，一般不需要修改
'DB_PREFIX'=>'cp_',//数据库前缀
'DB_PCONNECT'=>false,//true表示使用永久连接，false表示不适用永久连接，一般不使用永久连接
//模板配置
'TPL_TEMPLATE_PATH'=>'./template/',//模板目录，一般不需要修改
'TPL_TEMPLATE_SUFFIX'=>'.html',//模板后缀，一般不需要修改
'TPL_CACHE_ON'=>true,//是否开启模板缓存，true开启,false不开启

);
?>