<?php
return array(		
//模块配置
'MODULE_PATH'=>'./module/manage/',//模块存放目录，一般不需要修改
//模板配置
'TPL_TEMPLATE_PATH'=>'./template/manage/',//模板目录，一般不需要修改
//指定成功页面和错误页面模板
'TPL_ACTION_SUCCESS'=>'success',//成功页面模板
'TPL_ACTION_ERROR'=>'error',//错误页面模板
);
?>