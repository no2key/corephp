<?php
class guanggaoMod extends base{
	/**
	 * 列表页
	 *
	 */
	public function index(){
		$db = model('guanggao');
		//分页
		$page=new Page();
		$page->pageSuffix=cpConfig::get('URL_HTML_SUFFIX');
		$listRows=20;//每页显示的信息条数
		$cur_page=$page->getCurPage();
		$limit_start=($cur_page-1)*$listRows;
		$limit=$limit_start.','.$listRows;
		
		$_GET['weizhi'] && $where['weizhi']=intval($_GET['weizhi']);
		//信息总数
		$count = $db->where($where)->count();
		//当页信息列表
		$result = $db->where($where)->limit($limit)->order('id desc')->select();

		$array = array(
		'list'=>$result,
		);
		$this->assign($array);
		
		$this->display();
	}
	/**
	 * 添加公告界面
	 *
	 */
	public function add(){		
		if ($_POST) {
			$this->do_add();
			exit;
		}
		
		$this->display();
	}
	/**
	 * 添加公告操作
	 *
	 */
	public function do_add(){	
		
		//模型
		$db = model('guanggao');
		$data = $db->checkData();
		$error = $db->getError();
		if ($error) {
			$this->error($error);
		}
		$data['img'] = $this->_upfile();
		$data['uid'] = $this->_G['member']['uid'];
		$id = $db->data($data)->insert();

		if ($id) {		
			$this->success('公告发布成功');
		}else {
			$this->error('添加公告失败，请重新添加');
		}	
	}
	/**
	 * 删除公告
	 *
	 */
	public function del(){		
		$id = intval($_GET['id']);
		if ($id) {
			$del[] = $id;
		}else {
			$del = $_POST['id'];
		}
		$db = model('guanggao');
		if ($del) {
			foreach ($del as $val){
				$db->where("id=$val")->delete();
			}
		}
		$this->success('删除完毕');
	}
	
	public function edit(){
		if ($_POST) {
			$this->do_edit();
			exit;
		}		
		$db = model('guanggao');
		$where['id'] = intval($_GET['id']);
		$guanggao = $db->where($where)->find();		
		if ($guanggao) {
			$this->assign('guanggao',$guanggao);
			$this->display();
		}else {
			$this->error('找不到您要修改的公告');
		}
	}
	
	public function do_edit(){
		
		//公告表模型
		$db = model('guanggao');		
		$where['id'] = intval($_POST['id']);
		$data['title'] = $_POST['title'];
		$data['content'] = $_POST['content'];
		$db->where($where)->data($data)->update();
		$this->success('修改成功',url('guanggao/index'));
		
	}
	
	/**
	 * 文件上传
	 *
	 */
	protected  function _upfile(){
		$upload = new UploadFile();// 实例化上传类
		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
		$upload->savePath =  'upfile/guanggao/'.date("Y/m/d/",time());// 设置附件上传目录
		FS::s_mkdir($upload->savePath);
		if(!$upload->upload()) {// 上传错误提示错误信息
			$msg = $upload->getErrorMsg();
			$this->error($msg);
			/*$result['status']=0;
			$result['msg'] = $msg;	*/		
		}else{// 上传成功
			$imginfo=$upload->getUploadFileInfo();
			/*$result['status']=1;
			$result['msg'] = $imginfo;*/	
			return $imginfo['img']['savepath'].$imginfo['img']['savename'];			
		}
		
	}
}