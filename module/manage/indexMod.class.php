<?php
class indexMod extends base{
	public function index(){
		//echo url('manage/member/add');
		$str = date('Y-m-d',time());
		$url = "?start={$str}&end={$str}";
		if ($this->_G['member']['usergroup']==1) {
			$this->redirect(url('estate/index',array('type'=>1)).$url);
		}else {
			$this->redirect(url('userestate/index',array('type'=>1)));
		}


	}
	public function login(){
		$db = model('guanggao');
		$left = $db->where(array('weizhi'=>1))->select();
		$right = $db->where(array('weizhi'=>2))->select();
		$bottom = $db->where(array('weizhi'=>3))->select();
		$this->assign('left',$left);
		$this->assign('right',$right);
		$this->assign('bottom',$bottom);
		$this->display();
	}
	public function do_login(){
		$db = model('member');
		$where['username']=$_POST['username'];
		/*if (is_numeric($where['username'])) {
			$where['username'] = sprintf("%1$03d",$where['username']);
		}*/
		$member = $db->where($where)->find();
		if($member['password']==md5(md5($_POST['password'].cpConfig::get('ALL_PASS')).$member['salt'])){
			unset($member['salt']);
			$user_agent = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);//用户识别标记
			$member['user_agent']=$user_agent;//防止用户盗用cookie
			$cookie = cp_encode($member,cpConfig::get('authkey'));
			$this->set_cookie('member',$cookie);
			if ($member['usergroup']==1) {
				$this->redirect(url('estate/index'));
			}else {
				$this->redirect(url('userestate/index'));
			}
		}else{
			$this->error('登录失败，请重新登录',url('index/login'));
		}
	}
	public function logout(){
		$this->set_cookie('member','',$this->_G['time']-100);
		$this->redirect(__APP__);
	}
	/**
	 * excel 导入
	 *
	 * @param string $file
	 * @return array
	 */
	public function import(){
		//房源导入时间限制

		$chuzu = model('home_chuzu');
		$chushou = model('home_chushou');

		$type = $_GET['type'] ? $_GET['type'] : 'chuzu';


		$db = model();
		$apartment = $db->table('apartment')->select();//户型
		$certificate = $db->table('certificate')->select();//证书
		$decoration = $db->table('decoration')->select();//装修
		$structure = $db->table('structure')->select();// 结构
		$i = 1;//今日录入统计


		if ($type == 'chuzu') {
			$start = $_GET['start'];
			$mydata = $chuzu->order('createtime asc')->limit("$start , 5000")->select();
			if (!$mydata) {
				exit('chuzu success');
			}
			foreach ($mydata as $val){
				$val = $this->_htmlspecialchars($val);
				$insert['apartment']=$this->_property($val['huxing'],$apartment,'apartment');
				$insert['apartment_content']=$val['huxing'];
				$insert['decoration']=$this->_property($val['zhuangxiu'],$decoration,'decoration');
				$insert['address']=$val['didian'];
				$insert['floor']=$val['louceng'];
				$insert['area']=$val['mianji'];
				$insert['remark']=$val['beizhu'];
				$insert['rent']=$val['yuezu'];
				$insert['netid']=$val['wanghao'];
				$insert['rented']=0;

				$time = $val['createtime'];
				$insert['uid']=$this->_G['member']['uid'];
				$insert['type']=1;
				$insert['dateline'] = $time;
				$insert['year']=date('Y',$time);
				$insert['month']=date('n',$time);
				$insert['day']=date('j',$time);
				if(!empty($insert['netid'])) $db->table('estate')->data($insert)->insert();
				//更新统计
				$data = array(
				'year'=>date('Y',$time),
				'month'=>date('n',$time),
				'day'=>date('j',$time),
				'username'=>$insert['netid'],
				'num'=>1
				);
				$db->table('count')->data($data)->insert();
				//更新统计end

			}

		}

		if ($type == 'chushou') {
			$start = $_GET['start'];
			$mydata = $chushou->order('createtime asc')->limit("$start , 5000")->select();
			if (!$mydata) {
				exit('chushou success');
			}
			foreach ($mydata as $val){
				$val = $this->_htmlspecialchars($val);
				$insert['apartment']=$this->_property($val['huxing'],$apartment,'apartment');
				$insert['apartment_content']=$val['huxing'];
				$insert['decoration']=$this->_property($val['zhuangxiu'],$decoration,'decoration');
				$insert['certificate']=$this->_property($val['quanshu'],$certificate,'certificate');//
				$insert['structure']=$this->_property($val['jiegou'],$structure,'structure');//
				$insert['address']=$val['didian'];
				$insert['floor']=$val['louceng'];
				$insert['area']=$val['mianji'];
				$insert['remark']=$val['beizhu'];
				$insert['rent']=$val['shoujia'];
				//$insert['netid']=$val[10];
				$insert['netid']=$val['wanghao'];
				$insert['rented']=0;

				$time = $val['createtime'];
				$insert['uid']=$this->_G['member']['uid'];
				$insert['type']=2;
				$insert['dateline'] = $time;
				$insert['year']=date('Y',$time);
				$insert['month']=date('n',$time);
				$insert['day']=date('j',$time);
				if(!empty($insert['netid'])) $eid=$db->table('estate')->data($insert)->insert();
				//更新统计
				$data = array(
				'year'=>date('Y',$time),
				'month'=>date('n',$time),
				'day'=>date('j',$time),
				'username'=>$insert['netid'],
				'eid'=>$eid,
				'num'=>1
				);
				$db->table('count')->data($data)->insert();
				//更新统计end

			}
		}
		//删除空数据
		$db->table('estate')->where("netid=''")->delete();
		$db->table('count')->where("username=''")->delete();
		$start2 = $start+5000;
		$this->success("完成$start2",url('index/import',array('type'=>$type,'start'=>$start2)));
		exit;


	}

	/*//户型 装修等属性
	private function _property($str,&$arr,$model){
	if (empty($str)) {
	return 0;
	}
	//检测是否存在
	if (is_array($arr)) {
	foreach ($arr as $val){
	if (trim($str) == trim($val['name'])) {
	return $val['id'];
	}
	}
	}
	return 0;
	}*/
	//自动添加属性
	private function _property($str,&$arr,$model){
		if (empty($str)) {
			return '';
		}
		//检测是否存在
		if (is_array($arr)) {
			foreach ($arr as $val){
				if (trim($str) == trim($val['name'])) {
					return $val['id'];
				}
			}
		}
		//插入
		$db=model($model);
		$insert = array(
		'name'=>$str,
		'display'=>1,
		);
		$insert_id = $db->data($insert)->insert();
		//新插入数据保存到变量
		$arr[] = array(
		'id'=>$insert_id,
		'name'=>$str,
		'display'=>1,
		);
		return $insert_id;
	}
	
	public function user(){
		$db = model('user_list');
		$m = model('member');
		$mf = model('member_field');
		$olduser = $db->select();
		foreach ($olduser as $val) {
			$insert['salt'] = random();
			$insert['username'] = $val['username'];
			$insert['password'] = md5($val['password'].$insert['salt']);
			$uid = $m->data($insert)->insert();
			
			$mf->data(array('uid'=>$uid,'shop'=>$val['username']))->insert();
		}
		echo "success";
	}
	
	public function username(){
		$db = model('member');
		
		$olduser = $db->select();
		foreach ($olduser as $val) {
			if($val['username'][0]=='0'){
				$user = substr($val['username'],1);
				if ($user[0]=='0') {
					$user = substr($user,1);
				}
			}else {
				$user = $val['username'];
			}
			$data['username'] = $user;
			$where['uid'] = $val['uid'];
			$db->where($where)->data($data)->update();
			
		}
		echo "success";
		
	}
}