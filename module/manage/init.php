<?php
class base extends Action{
	protected $_G = array();

	/**
	 * 对数据进行处理
	 *
	 */
	protected function init(){
		if (isset($_GET)) $_GET = $this->_htmlspecialchars($_GET);
		if (isset($_POST)) $_POST = $this->_htmlspecialchars($_POST);
		if (isset($_REQUEST)) $_REQUEST = $this->_htmlspecialchars($_REQUEST);
		if (isset($_COOKIE)) $_COOKIE = $this->_htmlspecialchars($_COOKIE);
	}

	public function __construct(){
		timezone_set();
		$this->init();
		$this->check_login();//登录验证
		if (CP_ACTION != 'login' && CP_ACTION != 'do_login' && !$this->_G['member']['uid'] ) {
			$this->redirect(url('index/login'));
			//$this->error('请登录',url('index/login'));
		}
		$auth = $this->check_auth();//权限验证
		if (!$auth) {
			$this->error('你没有权限');
		}
		$this->assign('_G',$this->_G);

	}

	/**
	 * 验证登录状态
	 *
	 */
	protected function check_login(){
		$member = array();
		$cookie = $this->get_cookie('member');
		$user_agent = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);//用户识别标记
		$db = model();
		$member = cp_decode($cookie,cpConfig::get('authkey'));
		if($member['user_agent']==$user_agent){//检测cookie有效性，防止用户盗用cookie
			$find['uid'] = $member['uid'];
			$find['password'] = $member['password'];
			$have = $db->table('member')->where($find)->find();
		}
		if($have){
			unset($have['salt']);
			$this->_G['member'] = $have;
		}else{
			$this->set_cookie('member','',time () - 100);
			$this->_G['member'] = Array(
			'uid' => 0,
			'usergroup' => 4
			);
		}
	}
	/**
	 * 验证权限
	 *
	 * @return unknown
	 */
	protected function check_auth(){
		$config['AUTH_GROUP']=$this->_G['member']['usergroup'];
		$config['AUTH_ADMIN']=1;
		$config['AUTH_NO_CHECK']=array(
		'manage'=>array(
		'index'=>'*',
		),
		);
		RBAC::init($config);
		return RBAC::check();
	}
	/**
	 * 采用htmlspecialchars反转义特殊字符
	 *
	 * @param array|string $data 待反转义的数据
	 * @return array|string 反转义之后的数据
	 */
	protected function _htmlspecialchars(&$data) {
		return is_array($data) ? array_map(array($this, '_htmlspecialchars'), $data) : trim ( htmlspecialchars ( $data ) );
	}

	/**
	 * 判断是否为ajax传值
	 *
	 * @return unknown
	 */
	protected function isajax()
	{
		if (isset ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) && strtolower ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * cookie设置
	 *
	 * @param unknown_type $var
	 * @param unknown_type $value
	 * @param unknown_type $life
	 * @param unknown_type $prefix
	 */
	protected function set_cookie($var, $value, $life = 0, $prefix = 1)
	{
		$cookiepre = cpConfig::get('cookiepre');
		$cookiedomain = cpConfig::get('cookiedomain');
		$cookiepath = cpConfig::get('cookiepath');
		$timestamp = time();
		setcookie ( ($prefix ? $cookiepre : '') . $var, $value, $life ? $timestamp + $life : 0, $cookiepath, $cookiedomain, $_SERVER ['SERVER_PORT'] == 443 ? 1 : 0 );
	}

	/**
	 * 取得cookie
	 *
	 * @param unknown_type $var
	 * @return unknown
	 */
	protected function get_cookie($var)
	{
		$cookiename = cpConfig::get('cookiepre').$var;
		return $_COOKIE[$cookiename];
	}
}