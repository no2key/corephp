<?php
class newsMod extends base{
	/**
	 * 列表页
	 *
	 */
	public function index(){
		$db = model('news');
		//分页
		$page=new Page();
		$page->pageSuffix=cpConfig::get('URL_HTML_SUFFIX');
		$listRows=40;//每页显示的信息条数
		$cur_page=$page->getCurPage();
		$limit_start=($cur_page-1)*$listRows;
		$limit=$limit_start.','.$listRows;
		
		//信息总数
		$count = $db->count();
		//当页信息列表
		$result = $db->limit($limit)->order('id desc')->select();

		$array = array(
		'list'=>$result,
		);
		$this->assign($array);
		
		$this->display();
	}
	/**
	 * 添加公告界面
	 *
	 */
	public function add(){		
		if ($_POST) {
			$this->do_add();
			exit;
		}
		
		$this->display();
	}
	/**
	 * 添加公告操作
	 *
	 */
	public function do_add(){		
		//模型
		$db = model('news');
		$data = $db->checkData();
		$error = $db->getError();
		if ($error) {
			$this->error($error);
		}
		
		$data['uid'] = $this->_G['member']['uid'];
		$id = $db->data($data)->insert();
		if ($id) {		
			$this->success('公告发布成功');
		}else {
			$this->error('添加公告失败，请重新添加');
		}	
	}
	/**
	 * 删除公告
	 *
	 */
	public function del(){		
		$id = intval($_GET['id']);
		if ($id) {
			$del[] = $id;
		}else {
			$del = $_POST['id'];
		}
		$db = model('news');
		if ($del) {
			foreach ($del as $val){
				$db->where("id=$val")->delete();
			}
		}
		$this->success('删除完毕');
	}
	
	public function edit(){
		if ($_POST) {
			$this->do_edit();
			exit;
		}		
		$db = model('news');
		$where['id'] = intval($_GET['id']);
		$news = $db->where($where)->find();		
		if ($news) {
			$this->assign('news',$news);
			$this->display();
		}else {
			$this->error('找不到您要修改的公告');
		}
	}
	
	public function do_edit(){
		
		//公告表模型
		$db = model('news');		
		$where['id'] = intval($_POST['id']);
		$data['title'] = $_POST['title'];
		$data['content'] = $_POST['content'];
		$db->where($where)->data($data)->update();
		$this->success('修改成功',url('news/index'));
		
	}
	public function show(){
		$id = intval($_GET['id']);
		$db = model('news');
		$news = $db->where("id=".$id)->find();
		if ($news) {
			$this->assign('news',$news);
			$this->display();
		}else {
			$this->error('未找到您要查看的内容');
		}
	}
	
}