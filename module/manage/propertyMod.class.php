<?php
class propertyMod extends base{
	public function index(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : 'apartment';
		$db = model($model);

		$list = $db->order('display DESC,sort DESC')->select();
		$title = array(
		'apartment'=>'户型',
		'certificate'=>'权属',
		'decoration'=>'装饰类型',
		'structure'=>'房屋结构',
		'address'=>'搜索地址'
		);
		$array = array(
		'model'=>$model,
		'list'=>$list,
		'title'=>$title[$model],
		);
		$this->assign($array);
		$this->display();
	}
	public function add(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
		!$model &&  $this->error('参数错误');
		$title = array(
		'apartment'=>'户型',
		'certificate'=>'权属',
		'decoration'=>'装饰类型',
		'structure'=>'房屋结构',
		'address'=>'搜索地址'
		);
		$array = array(
		'model'=>$model,
		'title'=>$title[$model],
		);
		$this->assign($array);
		$this->display();
	}
	public function do_add(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
		!$model &&  $this->error('参数错误');
		$db = model($model);
		$data = $db->checkData();
		$error = $db->getError();
		if ($error) {
			$this->error($error);
		}
		$insert_id = $db->data($data)->insert();
		if ($insert_id) {
			$this->success('添加成功',url('property/index',"model=$model"));
		}else {
			$this->error('添加失败，请重试');
		}
	}
	public function edit(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
		!$model &&  $this->error('参数错误');
		$id = intval($_GET['id']);
		$db = model($model);
		$property=$db->where("id=$id")->find();
		$array = array(
		'model'=>$model,
		'property'=>$property
		);
		$this->assign($array);
		$this->display();
	}

	public function do_edit(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
		!$model &&  $this->error('参数错误');
		$db = model($model);		
		$_validate = array(
			array('name','require','请填写户型名称'),	
		);
		$data = $db->validate($_validate)->checkData();
		$error = $db->getError();
		if ($error) {		
			$this->error($error);
		}
		$where = array('id'=>$data['id']);
		unset($data['id']);
		$update = $db->where($where)->data($data)->update();
		if ($update) {
			$this->success('修改成功',url('property/index',"model=$model"));
		}else {
			$this->error('修改失败，请重新修改');
		}
	}
	/*public function do_display(){
	$where['id'] = intval($_GET['id']);
	$update['display'] = intval($_GET['display']);
	$model = $_GET['model'];
	$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
	!$model &&  $this->error('参数错误');
	$db = model($model);
	$db->where($where)->data($update)->update();
	$this->success('设置成功');

	}*/
	public function del(){
		$id = intval($_GET['id']);
		$model = $_GET['model'];
		if ($id) {
			$del[]=$id;
		}else {
			$del = $_POST['id'];
		}
		foreach ($del as $del_id){
			$where['id'] = intval($del_id);			
			$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
			!$model &&  $this->error('参数错误');
			
			$estate = model('estate');		
			$have = $estate->where($model.'='.$where['id'])->find();
			if ($have) {
				$this->error('属性被使用，请先删除相应房源后再删除属性');
			}
			
			$db = model($model);
			$db->where($where)->delete();
		}
		$this->success('删除成功');
	}
	
	public function searchdisplay(){
		$model = $_GET['model'];
		$model = in_array($model,array('apartment','certificate','decoration','structure','address')) ? $model : '';
		!$model &&  $this->error('参数错误');
		$id = intval($_GET['id']);
		$db = model($model);
		$data['display'] = intval($_GET['display']);
		$property=$db->where("id=$id")->data($data)->update();
		$this->success('设置成功');
	}

}