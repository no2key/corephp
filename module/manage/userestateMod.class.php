<?php
class userestateMod extends base{
	public function __construct(){
		parent::__construct();
		$this->auto_status();//自动审核
	}
	public function index(){
		$type = intval($_GET['type']) ? intval($_GET['type']) : 1;
		//分页
		$page=new Page();
		$page->pageSuffix=cpConfig::get('URL_HTML_SUFFIX');
		$listRows=40;//每页显示的信息条数
		$cur_page=$page->getCurPage();
		$limit_start=($cur_page-1)*$listRows;
		$limit=$limit_start.','.$listRows;

		$db = model();
		//属性
		$apartment = $this->getProperty('apartment');//户型
		$certificate = $this->getProperty('certificate');//证书
		$decoration = $this->getProperty('decoration');//装修
		$structure = $this->getProperty('structure');// 结构
		$address = $this->getProperty('address');// 地址

		//房源
		$table = "estate";
		$where = " type=".$type;
		$where .= $this->sql();//取得查询条件
		if (empty($_GET['start'])&&empty($_GET['end']) && empty($_GET['netid']) ) {//如果没有选择搜索日期，默认显示到昨天的记录
			$timestr = date('Y-m-d',time());
			$timestr = strtotime($timestr);//今天凌晨的时间戳
			$qiantian = $timestr-(3600*24);
			//$yesterday = $timestr-(3600*24);
			$where .= " and dateline>$qiantian and dateline < $timestr";//昨天的数据
		}
		$order = 'dateline desc,apartment_content asc';
		if ($_GET['export']) {//导出
			$data = $db->table($table)->field($field)->where($where)->order($order)->select();
			if (!$data) $this->error('没有找到您要导出的数据');
			foreach ($data as $key => $val){
				$data[$key]['aname'] = $apartment[$val['apartment']]['name'];
				$data[$key]['dname'] = $decoration[$val['decoration']]['name'];
				$data[$key]['cname'] = $certificate[$val['certificate']]['name'];
				$data[$key]['sname'] = $structure[$val['structure']]['name'];
			}
			$this->export($data,$type);
			exit;
		}


		if($_GET['compare']){
			$order = "address ASC ,apartment ASC,decoration ASC, certificate ASC, decoration ASC,";
		}


		//信息总数
		$count = $db->table($table)->where($where)->count();
		//当页信息列表
		$result = $db->table($table)->field($field)->where($where)->limit($limit)->order($order)->select();
		
		if ($result) {
			foreach ($result as $key => $val){
				$result[$key]['aname'] = $apartment[$val['apartment']]['name'];
				$result[$key]['dname'] = $decoration[$val['decoration']]['name'];
				$result[$key]['cname'] = $certificate[$val['certificate']]['name'];
				$result[$key]['sname'] = $structure[$val['structure']]['name'];
			}
		}
		
		//获取行数
		$pagestring = $page->show($url,$count,$listRows,10,4);

		$this->assign('estate_count',$estate_count['num']);
		$this->assign('list',$result);
		$this->assign('type',$type);
		$this->assign('pagestring',$pagestring);		
		
		$this->assign('apartment',$apartment);
		$this->assign('certificate',$certificate);
		$this->assign('decoration',$decoration);
		$this->assign('structure',$structure);
		$this->assign('address',$address);
		if ($_GET['op']=='old') {
			$this->display('userestate/old');
		}else{
			$this->display();
		}
	}
	
	//搜索语句处理
	public function sql(){
		$where = '';
		//导出时使用
		if ($_GET['date']) {
			$day = intval($_GET['date']);
			$time = time();
			$time = strtotime(date('Y-m-d',$time));
			if($this->_G['member']['usergroup']==1){
				$time = $time - ( 3600*24*($day-1) );
				$where .= " and dateline>".$time;
			}else{//非管理员下载今天之前的数据
				$today = $time - (3600*24);
				$time = $time - ( 3600*24*($day-1) ) - (3600*24);
				$where .= " and dateline>".$time." and dateline<".$today;
			}
		}
		//列表页使用
		if ($_GET['apartment']) {
			$where .= " and apartment_content like '%{$_GET['apartment']}%'";;
		}

		if (!isset($_GET['apartment']) || $_GET['apartment']=='') {
			$apartment_content = '';
			intval($_GET['fang']) && $apartment_content .= intval($_GET['fang']).'房%';
			intval($_GET['ting']) && $apartment_content .= intval($_GET['ting']).'厅%';
			intval($_GET['wei']) && $apartment_content .= intval($_GET['wei']).'卫%';
			intval($_GET['chu']) && $apartment_content .= intval($_GET['chu']).'厨%';
			intval($_GET['yang']) && $apartment_content .= intval($_GET['yang']).'阳%';
			if ($apartment_content) {
				$apartment_content = '%'.$apartment_content;
				$where .= " and apartment_content like '{$apartment_content}'";
			}
		}
		//面积
		switch ($_GET['area']){
			case 1:
				$where .= " and area between 0 and 30";
				break;
			case 2:
				$where .= " and area between 30 and 60";
				break;
			case 3:
				$where .= " and area between 60 and 90";
				break;
			case 4:
				$where .= " and area between 90 and 120";
				break;
			case 5:
				$where .= " and area between 120 and 150";
				break;
			case 6:
				$where .= " and area > 150";
				break;
			default:
		}
		//时间
		if ($_GET['start']) {
			$where .= " and dateline >".strtotime($_GET['start']);
		}
		if ($_GET['end']) {
			$end = strtotime($_GET['end']);
			$end = $end+(3600*24);
			$where .= " and dateline <".$end;
		}
		//价格
		if ($_GET['min']) {
			$where .= " and rent >".intval($_GET['min']);
		}
		if ($_GET['max']) {
			$where .= " and rent <".intval($_GET['max']);
		}
		//网号
		if($_GET['netid']){
			$where .= " and netid='{$_GET['netid']}'";

		}
		//地址
		if ($_GET['address'] && $_GET['address'] != '输入关键字(楼盘名或地段等)') {
			$where .= " and address like '%{$_GET['address']}%'";
		}
		return $where;
	}
	public function search(){
		$this->display();
	}
	/**
	 * excel 导入
	 *
	 * @param string $file
	 * @return array
	 */
	public function import(){
		

		if($_FILES){
			//房源导入时间限制
			if ($this->_G['member']['usergroup'] != 1) {
				$this->timelimit();
			}
			$type = intval($_POST['type']);
			$file=$this->_upFile();
			$excel = $this->_excelToArray($file);

			$db = model();
			$apartment = $db->table('apartment')->select();//户型
			$certificate = $db->table('certificate')->select();//证书
			$decoration = $db->table('decoration')->select();//装修
			$structure = $db->table('structure')->select();// 结构
			$i = 1;//今日录入统计
			$time = time();
			//检测网号是否正确
			foreach ($excel as $key=>$val){
				if ($type==1) {
					if(is_null($val['8'])) break;
					$inexcel[] = $val;
					if($val[8] != $this->_G['member']['username']){
						$this->error('网号有错误，请核对后上传');
					}
				}
				if ($type==2) {
					if(is_null($val['10'])) break;
					$inexcel[] = $val;
					if ($val[10] != $this->_G['member']['username']) {
						$this->error('网号有错误，请核对后上传');
					}
				}
			}
			foreach($inexcel as $key => $val){
				$val = $this->_htmlspecialchars($val);
				if ($type==1) {
					$insert['apartment']=$this->_property($val[3],$apartment,'apartment');
					$insert['apartment_content']=$val[3];
					$insert['decoration']=$this->_property($val[5],$decoration,'decoration');
					$insert['address']=$val[1];
					$insert['floor']=$val[2];
					$insert['area']=$val[4];
					$insert['remark']=$val[6];
					$insert['rent']=$val[7];
					//$insert['netid']=$val[8];
					$insert['netid']=$this->_G['member']['usergroup']==1 ? $val[8] : $this->_G['member']['username'];
					$insert['rented']=$val[9];
				}
				if ($type==2) {
					$insert['apartment']=$this->_property($val[3],$apartment,'apartment');
					$insert['apartment_content']=$val[3];
					$insert['decoration']=$this->_property($val[7],$decoration,'decoration');
					$insert['certificate']=$this->_property($val[5],$certificate,'certificate');//
					$insert['structure']=$this->_property($val[6],$structure,'structure');//
					$insert['address']=$val[1];
					$insert['floor']=$val[2];
					$insert['area']=$val[4];
					$insert['remark']=$val[8];
					$insert['rent']=$val[9];
					//$insert['netid']=$val[10];
					$insert['netid']=$this->_G['member']['usergroup']==1 ? $val[10] : $this->_G['member']['username'];
					$insert['rented']=$val[11];
				}
				$insert['uid']=$this->_G['member']['uid'];
				$insert['type']=$type;
				$insert['dateline'] = $time;
				$insert['year']= date('Y',$time);
				$insert['month'] = date('n',$time);
				$insert['day'] = date('j',$time);
				if(!empty($insert['netid'])) $eid=$db->table('estate')->data($insert)->insert();
				
				//更新统计
				$data = array(
				'eid'=>$eid,
				'year'=>date('Y',$time),
				'month'=>date('n',$time),
				'day'=>date('j',$time),
				'username'=>$insert['netid'],
				'num'=>1
				);
				$db->table('count')->data($data)->insert();	
				
				//更新统计end
			}
			//删除空数据
			$db->table('estate')->where("netid=''")->delete();
			$db->table('count')->where("username=''")->delete();
			$this->success('导入成功');
			exit;
		}

		//统计
		$time = time();
		$condition = array(
		'year'=>date('Y',$time),
		'month'=>date('n',$time),
		'day'=>date('j',$time),
		'username'=>$this->_G['member']['username'],
		);
		$db = model();
		$estate_count = $db->table('count')->where($condition)->count();
		$this->assign('today_count',$estate_count);
		$type = intval($_GET['type']) ? intval($_GET['type']) : 1;
		$all_where = array(
		'year'=>date('Y',$time),
		'month'=>date('n',$time),
		'username'=>$this->_G['member']['username'],
		);
		$all_count = $db->table('count')->where($all_where)->count();
		$this->assign('all_count',$all_count);
		$this->assign('type',$type);
		$this->display();
	}
	/*//户型 装修等属性
	private function _property($str,&$arr,$model){
	if (empty($str)) {
	return 0;
	}
	//检测是否存在
	if (is_array($arr)) {
	foreach ($arr as $val){
	if (trim($str) == trim($val['name'])) {
	return $val['id'];
	}
	}
	}
	return 0;
	}*/
	//自动添加属性
	private function _property($str,&$arr,$model){
		if (empty($str)) {
			return '';
		}
		//检测是否存在
		if (is_array($arr)) {
			foreach ($arr as $val){
				if (trim($str) == trim($val['name'])) {
					return $val['id'];
				}
			}
		}
		//插入
		$db=model($model);
		$insert = array(
		'name'=>$str,
		'display'=>0,
		);
		$insert_id = $db->data($insert)->insert();
		//新插入数据保存到变量
		$arr[] = array(
		'id'=>$insert_id,
		'name'=>$str,
		'display'=>0,
		);
		return $insert_id;
	}
	//取得属性并用id作为key
	private function getProperty($model='apartment'){
		$db = model($model);
		$reslut = $db->order('name ASC')->select();
		if (!$reslut) return ;
		foreach ($reslut as $val){
			$return[$val['id']] = $val;
		}
		return $return;
	}
	/**
	 * excel 导出
	 *
	 * @param string $file
	 * @return array
	 */
	public function export($data,$type){
		if ($type==1) {
			$excel[] = array(
			array('val'=>'地址','align'=>'center','width'=>50),
			array('val'=>'楼层','align'=>'center','width'=>10),
			array('val'=>'户型','align'=>'center','width'=>10),
			array('val'=>'面积','align'=>'center','width'=>10),
			array('val'=>'装修','align'=>'center','width'=>10),
			array('val'=>'备注','align'=>'center','width'=>50),
			array('val'=>'月租','align'=>'center','width'=>10),
			array('val'=>'网号','align'=>'center','width'=>10),
			array('val'=>'租否','align'=>'center','width'=>10),
			);

			foreach($data as $key=>$val){
				$excel[] = array(
				array('val'=>$val['address']),
				array('val'=>$val['floor']),
				array('val'=>$val['aname']),
				array('val'=>$val['area']),
				array('val'=>$val['dname']),
				array('val'=>$val['remark']),
				array('val'=>$val['rent']),
				array('val'=>$val['netid']),
				array('val'=>$val['rented'] ? '是':'否'),
				);
			}
			$title = "出租房源";
		}
		if ($type==2) {
			$excel[] = array(
			array('val'=>'地址','align'=>'center','width'=>50),
			array('val'=>'楼层','align'=>'center','width'=>10),
			array('val'=>'户型','align'=>'center','width'=>10),
			array('val'=>'面积','align'=>'center','width'=>10),
			array('val'=>'权属','align'=>'center','width'=>10),
			array('val'=>'结构','align'=>'center','width'=>10),
			array('val'=>'装修','align'=>'center','width'=>10),
			array('val'=>'备注','align'=>'center','width'=>50),
			array('val'=>'售价','align'=>'center','width'=>10),
			array('val'=>'网号','align'=>'center','width'=>10),
			array('val'=>'售否','align'=>'center','width'=>10),
			);

			foreach($data as $key=>$val){
				$excel[] = array(
				array('val'=>$val['address']),
				array('val'=>$val['floor']),
				array('val'=>$val['aname']),
				array('val'=>$val['area']),
				array('val'=>$val['cname']),
				array('val'=>$val['sname']),
				array('val'=>$val['dname']),
				array('val'=>$val['remark']),
				array('val'=>$val['rent']),
				array('val'=>$val['netid']),
				array('val'=>$val['rented'] ? '是':'否'),
				);
			}
			$title = "出售房源";
		}
		$putout = new ExcelExport($title);
		foreach($excel as $val){
			$putout->setCells($val);
		}
		$putout->save();

	}
	/**
     * 房源审核
     *
     
	public function status(){
		$id = intval($_GET['id']);
		if ($id) {
			$status[]=$id;
		}else {
			$status = $_POST['id'];
		}
		$db = model('estate');
		foreach ($status as $val){
			$id = intval($val);
			$where['id']=$id;
			if ($this->_G['member']['usergroup'] != 1){
				$where['uid']=$this->_G['member']['uid'];
			}
			$where = "id=".$id;
			$update['status']=1;
			$db->where($where)->data($update)->update();
		}
		$this->success('审核成功');
	}*/



	
	//图片上传
	private function _upFile(){

		/*上传文件处理*/
		$upload = new UploadFile();// 实例化上传类

		$upload->maxSize  = 3145728 ;// 设置附件上传大小
		$upload->allowExts  = array('xls', 'xlsx');// 设置附件上传类型

		//$upload->saveRule = time().'-'.random(4,true); //生成文件名称
		$upload->savePath =  'upfile/';// 设置附件上传目录

		if(!$upload->upload()) {// 上传错误提示错误信息
			$error = $upload->getErrorMsg();
			$this->error($error);
			return false;
		}else{// 上传成功
			$excel=$upload->getUploadFileInfo();
			$insert = array(
			'filename'=>$excel['excel']['name'],
			'savepath'=>$excel['excel']['savepath'],
			'savename'=>$excel['excel']['savename'],
			'size'=>$excel['excel']['size'],
			'uid'=>$this->_G['member']['uid'],
			'dateline'=>time()
			);
			$db = model('file');
			$db->data($insert)->insert();
			return $excel['excel']['savepath'].$excel['excel']['savename'];

		}
		/*上传文件处理结束*/
	}
	public function add(){
		
		if ($_POST) {
			//房源导入时间限制
			if ($this->_G['member']['usergroup'] != 1) {
				$this->timelimit();
			}
			$this->do_add();
			exit;
		}
		$type = intval($_GET['type']) ? intval($_GET['type']) : 1;

		$array=array(
		'type'=> $type,
		'apartment' => model('apartment')->where("display=1")->order("sort DESC")->select(),
		'certificate' => model('certificate')->where("display=1")->order("sort DESC")->select(),
		'decoration' => model('decoration')->where("display=1")->order("sort DESC")->select(),
		'structure' => model('structure')->where("display=1")->order("sort DESC")->select(),
		);
		$this->assign($array);
		$this->display();
	}
	public function do_add(){
		$db = model('estate');
		$rule = array(
		array('address','require','请填写地址'),
		array('floor','require','请填写楼层'),
		array('area','require','请填写面积'),
		);
		$insert = $db->validate($rule)->checkData();
		$error = $db->getError();
		if ($error) {
			$this->error($error);
		}
		$insert['uid']=$this->_G['member']['uid'];
		$insert['dateline'] = time();
		if ($this->_G['member']['usergroup'] != 1) {
			$insert['netid']=$this->_G['member']['username'];
		}
		//户型
		$insert['apartment_content']=model('apartment')->where("id=".intval($_POST['apartment']))->find();
		$insert['apartment_content']=$insert['apartment_content']['name'];

		if(!$insert['netid']) {
			$this->error('请填写网号');
		}
		$db->data($insert)->insert();

		//更新统计
		$time = time();
		$where = array(
		'year'=>date('Y',$time),
		'month'=>date('n',$time),
		'day'=>date('j',$time),
		'username'=>$insert['netid'],
		);
		$have=$db->table('count')->where($where)->find();
		if ($have) {
			$data['num']=$have['num']+1;
			$db->table('count')->where($where)->data($data)->update();
		}else{
			$data = $where;
			$data['num']=1;
			$db->table('count')->data($data)->insert();
		}
		$this->success('添加成功');
	}
	public function edit(){
		if ($_POST) {
			$this->do_edit();
			exit;
		}
		$where['id'] = intval($_GET['id']);
		$where['netid']=$this->_G['member']['username'];

		$db = model('estate');
		$estate = $db->where($where)->find();
		if(!$estate){
			$this->error('你的记录里找不到该条数据');
		}
		$array=array(
		'type'=>intval($_GET['type']),
		'estate'=> $estate,
		'apartment' => model('apartment')->where("display=1")->order("sort DESC")->select(),
		'certificate' => model('certificate')->where("display=1")->order("sort DESC")->select(),
		'decoration' => model('decoration')->where("display=1")->order("sort DESC")->select(),
		'structure' => model('structure')->where("display=1")->order("sort DESC")->select(),
		);
		$this->assign($array);
		$this->display();
	}
	public function do_edit(){
		$where['id'] = intval($_POST['id']);
		$where['netid']=$this->_G['member']['username'];
		$where['status']=0;

		$db = model('estate');
		$rule = array(
		array('address','require','请填写地址'),
		array('floor','require','请填写楼层'),
		array('area','require','请填写面积'),
		);
		$update = $db->validate($rule)->checkData();
		unset($update['id']);
		$error = $db->getError();
		if ($error) {
			$this->error($error);
		}

		$db->where($where)->data($update)->update();
		
		$this->success('修改成功');

	}
	/**
	 * 删除
	 *
	 */
	public function del(){
		$del[]=intval($_GET['id']);
		if ($_POST) {
			$del=$_POST['id'];
		}
		$db = model('estate');
		$count = model('count');
		foreach ($del as $val){
			$where['id'] = intval($val);
			if ($this->_G['member']['usergroup'] != 1) {
				$where['netid']=$this->_G['member']['username'];
				$where['status']=0;
			}
			$db->where($where)->delete();
			$count->where("eid=".$where['id'])->delete();//删除统计信息
		}
		$this->success('删除成功');
	}
	public function refresh(){
		$id = intval($_GET['id']);
		if ($id) {
			$refresh[]=$id;
		}else {
			$refresh = $_POST['id'];
		}
		$db = model('estate');
		$count = model('count');
		
		$where = array();
		foreach ($refresh as $val){
			$where['id'] = intval($val);
			if ($this->_G['member']['usergroup'] != 1) {
				$where['netid']=$this->_G['member']['username'];
			}
			//$update['dateline']=time();
			//$status = $db->where($where)->data($update)->update();
			//$db->where($where)->data("refresh = refresh+1")->update();//更新刷新次数
			//更新统计
			$estate = $db->where($where)->find();
			$new = $estate;
			$new['dateline']=time();
			$new['status']=0;
			unset($new['id']);
			$eid = $db->data($new)->insert();	
			if($estate){
				$time = time();
				$count_data = array(
				'eid'=>$eid,
				'year'=>date('Y',$time),
				'month'=>date('n',$time),
				'day'=>date('j',$time),
				'username'=>$estate['netid'],
				'num'=>1
				);
				$count->data($count_data)->insert();//更新成功增添记录条数
			}

		}
		
		$this->success('重新发布成功');
	}
	public function rented(){
		$update['rented'] = intval($_GET['rented']);

		if ($_POST['id']) {
			$db = model('estate');
			foreach ($_POST['id'] as $id){
				$where['id']=intval($id);
				$where['netid']=$this->_G['member']['username'];
				$db->where($where)->data($update)->update();

			}
		}
		$this->success('更新成功');
	}
	/**
	 * excel读取返回数组
	 *
	 * @param string $file
	 * @return array
	 */
	private function _excelToArray($file){
		require_once CP_PATH.'ext/PHPExcel/IOFactory.php';
		$extend = pathinfo($file);
		$extend = strtolower($extend["extension"]);
		$reader_type = array(
		'xls'=> "Excel5",
		'xlsx'=>"Excel2007",
		);
		/*require_once 'Lib/ORG/Classes/PHPExcel/IOFactory.php';
		$reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
		$PHPExcel = $reader->load("1.xlsx"); // 载入excel文件*/
		$PHPExcel = PHPExcel_IOFactory::load($file);
		$sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
		$highestRow = $sheet->getHighestRow(); // 取得总行数
		$highestColumm = $sheet->getHighestColumn(); // 取得总列数
		$highestColumm = 'L';
		/** 循环读取每个单元格的数据 */
		for ($row = 3; $row <= $highestRow; $row++){//行数是以第2行开始
			for ($column = 'A'; $column <= $highestColumm; $column++) {//列数是以A列开始
				$dataset[$row][] = $sheet->getCell($column.$row)->getValue();
				//echo $column.$row.":".$sheet->getCell($column.$row)->getValue()."<br />";
			}
		}
		return $dataset;
	}
	//添加导入房源时间限制
	public function timelimit(){
		$time = time();
		$hour = date('G',$time);//小时
		$minute= date('i',$time);//分
		$week = date('w',$time);//星期几
		if ($week==0) {//星期天
			if ($hour<8 or $hour>14) {
				$this->error('对不起现在不可以导入房源');
			}
		}else{//周一至周六
			if($hour<8 or $hour>20) $this->error('对不起现在不可以导入房源');
			if ($hour==20 && $minute>=30) {
				$this->error('对不起现在不可以导入房源');
			}
		}
	}
	/**
	 * 自动审核
	 *
	 */
	private function auto_status(){
		$time = time();
		$hour = date('G',$time);//小时
		$minute= date('i',$time);//分
		$week = date('w',$time);//星期几
		$timestr = date('Y-m-d',$time);

		$timestr = strtotime($timestr);//今天凌晨的时间戳
		$where = "dateline < $timestr";//昨天的数据
		$update['status']=1;
		$db = model('estate');
		//当天数据自动审核
		if ($week==0) {//星期天
			if ($hour >13) {
				$db->data($update)->update();
			}
		}else{//周一至周六
			if($hour >19) {
				$db->data($update)->update();
			}
		}
		//其他时间 昨天数据自动审核
		$db->data($update)->where($where)->update();
	}
	//今日房源
	public function today(){
		$type = intval($_GET['type']) ? intval($_GET['type']) : 1;
		//分页
		$page=new Page();
		$page->pageSuffix=cpConfig::get('URL_HTML_SUFFIX');
		$listRows=40;//每页显示的信息条数
		$cur_page=$page->getCurPage();
		$limit_start=($cur_page-1)*$listRows;
		$limit=$limit_start.','.$listRows;

		$db = model();
		//属性
		$apartment = $this->getProperty('apartment');//户型
		$certificate = $this->getProperty('certificate');//证书
		$decoration = $this->getProperty('decoration');//装修
		$structure = $this->getProperty('structure');// 结构
		
		$where = " type=".$type;

		$timestr = date('Y-m-d',time());
		$timestr = strtotime($timestr);//今天凌晨的时间戳
		//$yesterday = $timestr-(3600*24);
		$where .= " and dateline > $timestr";//昨天的数据
		$where .= " and netid='{$this->_G['member']['username']}'";
		
		$order = 'apartment_content asc,dateline desc';
		$table = 'estate';
		//信息总数
		$count = $db->table($table)->where($where)->count();
		//当页信息列表
		$result = $db->table($table)->field($field)->where($where)->limit($limit)->order($order)->select();
		if ($result) {
			foreach ($result as $key => $val){
				$result[$key]['aname'] = $apartment[$val['apartment']]['name'];
				$result[$key]['dname'] = $decoration[$val['decoration']]['name'];
				$result[$key]['cname'] = $certificate[$val['certificate']]['name'];
				$result[$key]['sname'] = $structure[$val['structure']]['name'];
			}
		}
		//获取行数
		$pagestring = $page->show($url,$count,$listRows,10,4);

		$this->assign('estate_count',$estate_count['num']);
		$this->assign('list',$result);
		$this->assign('type',$type);
		$this->assign('pagestring',$pagestring);
		
		$this->assign('apartment',$apartment);
		$this->assign('certificate',$certificate);
		$this->assign('decoration',$decoration);
		$this->assign('structure',$structure);

		$this->display();
	}

}